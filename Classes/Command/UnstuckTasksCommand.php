<?php

namespace Nerdost\NoStuckedScheduler\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UnstuckTasksCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setHelp('Unlocks all scheduler tasks which are stucked for a certain time in seconds.');
        $this->addArgument('maxRunTime', \Symfony\Component\Console\Input\InputArgument::REQUIRED, 'Maximum allowed task runtime in seconds');
    }

    /**
     * Executes the command for showing sys_log entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int error code
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        $maxDuration = (int) $input->getArgument('maxRunTime');

        if($maxDuration <= 0) {
            throw new \Exception('No maximum duration time given for unstuck scheduler task.');
        }

        $tasksRunningTooLong = [];

        /**
         * Inspired by EXT:schedulermonitor from coding.ms
         */

        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_scheduler_task');

        // Select all tasks with executions
        $result = $queryBuilder->select('uid', 'serialized_executions', 'serialized_task_object')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->neq(
                    'serialized_executions',
                    $queryBuilder->createNamedParameter('', \PDO::PARAM_STR)
                ),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute();

        while ($row = $result->fetch()) {
            if ($serialized_executions = unserialize($row['serialized_executions'])) {
                foreach ($serialized_executions as $taskStartTime) {
                    $durationSeconds = $GLOBALS['EXEC_TIME'] - $taskStartTime;
                    if ($durationSeconds > $maxDuration) {
                        $row['taskStartTime'] = $taskStartTime;
                        $tasksRunningTooLong[] = $row;
                    }
                }
            }
        }

        if(true === empty($tasksRunningTooLong)) {
            return Command::SUCCESS;
        }

        $scheduler = GeneralUtility::makeInstance(\TYPO3\CMS\Scheduler\Scheduler::class);

        foreach ($tasksRunningTooLong as $task) {
            $taskObj = $scheduler->fetchTask($task['uid']);
            if($taskObj->isExecutionRunning() === true) {
                $taskObj->unmarkAllExecutions();
            }
        }

        return Command::SUCCESS;
    }
}
