<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Detects stucked scheduler task and stops them after amount of time to be started again (e.g. in case of max_execution time limit and so on)',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Paul Beck',
    'author_email' => 'p.beck@nerdost.net',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
