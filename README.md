Short introduction
-- 
___

This extension provides a (scheduler) command to end frozen, currently running tasks. This means that these tasks can be restarted at the next cronjob call and are no longer skipped because they are frozen at "running".
The maximum runtime of other tasks can be configured / transferred to the command of this extension using an argument.

Thanks to our sponsors https://nerdost.net and https://www.medismedia.de/
And contributors from Git: BokuNoMaxi 